/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.css in this case)
require('../css/app.css');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
// var $ = require('jquery');

require('../../config.js');

import React from 'react';
import ReactDOM from 'react-dom';

import {DragDropContext} from 'react-beautiful-dnd';

import Message from '../components/Message.react';
import Board from '../components/Board.react';
import ClientApi from '../components/ClientApi.react';
import BoardHelper from '../components/BoardHelper.react';
import moment from 'moment';

class Priorities extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            boards: [],
            tasks: [],
            message: '',
            clock: {},
            full_screen: 1
        };
        this.addItem = this.addItem.bind(this);
        this.removeItem = this.removeItem.bind(this);
        this.onDragEnd = this.onDragEnd.bind(this);
        this.changeView = this.changeView.bind(this);
    }

    static getClockObject(time_now, time_last) {
        return {
            time_now: time_now,
            time_last: time_last,
            time_diff: time_now - time_last
        }
    }

    static createTask(title, category_id, position) {
        return {
            title: title,
            position: position,
            category: {
                id: category_id
            }
        }
    }

    componentDidMount() {
        ClientApi.getAllTasks().then(data => {
            this.setState({tasks: data});
        });

        ClientApi.getLastTimePriorityChange().then(result => {
            let clock = Priorities.getClockObject(moment().unix(), moment(result.last_date).unix());
            console.log(clock);
            this.setState({clock: clock});
        });


        this.intervalID = setInterval(
            () => ClientApi.getAllTasks().then(data => {
                this.setState({tasks: data});
            }),
            5000
        );
    }

    changeView(e) {
        if (this.state.full_screen) {
            this.setState({full_screen: 0});
        } else {
            this.setState({full_screen: 1});
        }
    }

    addItem(e) {
        if (this._inputElement.value !== "") {
            let newTask = Priorities.createTask(this._inputElement.value, 1, this.state.tasks.length + 1);

            ClientApi.addTask(newTask).then(result => {
                if (result.status !== 500) {
                    return result.json();
                } else {
                    this.setState({message: 'Something went wrong'});
                }
            }).then(data => {
                this.setState((prevState) => {
                    return {
                        tasks: prevState.tasks.concat(data)
                    }
                });
                this._inputElement.value = "";
            });

            e.preventDefault();
        }
    }

    removeItem(e, task) {
        ClientApi.removeTask(task).then(result => {
            if (result.status !== 500) {
                let update_tasks = this.state.tasks.filter(function (item) {
                    return item.id !== task.id;
                });

                update_tasks.map((task, order) => {
                    task.position = order + 1;

                    ClientApi.updateTaskPosition(task).then(result => {
                        if (result.status !== 500) {
                            return result.json();
                        } else {
                            this.setState({message: 'Something went wrong'});
                        }
                    });
                });

                this.setState({tasks: update_tasks});
            } else {
                this.setState({message: 'Something went wrong'});
            }
        });

        e.preventDefault();
    }

    onDragEnd(result) {
        // dropped outside the list
        if (!result.destination) {
            return;
        }

        let tasks = BoardHelper.reorder(
            this.state.tasks,
            result.source.index,
            result.destination.index
        );

        tasks.map((task, order) => {
            task.position = order + 1;

            ClientApi.updateTaskPosition(task).then(result => {
                if (result.status !== 500) {
                    return result.json();
                } else {
                    this.setState({message: 'Something went wrong'});
                }
            });
        });

        //TODO: This does not work
        // ClientApi.createLastPriorityChange().then(result => {
        //     return result.json();
        // }).then(result => {
        //     this.setState({clock: Priorities.getClockObject(new Date(result.last_date).getTime(), new Date().getTime())});
        // });

        this.setState({tasks: tasks});
    }

    renderViewNormal(tasks) {
        return <div className="container">
            <div className="row">
                <div className="col-md-12">
                    <form onSubmit={this.addItem}>
                        <div className="form-row align-items-center">
                            <div className="col-auto">
                                <input className="form-control mb-4" ref={(a) => this._inputElement = a}
                                       placeholder="Add task"/>
                            </div>
                            <div className="col-auto">
                                <button className="btn btn-primary btn_add mb-4">Add</button>
                            </div>
                        </div>
                    </form>
                    <DragDropContext onDragEnd={this.onDragEnd}>
                        <Board tasks={tasks} removeItem={this.removeItem} changeView={this.changeView}
                               clock={this.state.clock}/>
                    </DragDropContext>
                </div>
            </div>
        </div>
    }

    renderViewFullScreen(tasks) {
        return <div className="col-md-12 full-screen">
            <DragDropContext onDragEnd={this.onDragEnd}>
                <Board tasks={tasks} removeItem={this.removeItem} changeView={this.changeView}
                       clock={this.state.clock}/>
            </DragDropContext>
        </div>
    }

    getView(tasks) {
        if (this.state.full_screen) {
            return this.renderViewFullScreen(tasks);
        } else {
            return this.renderViewNormal(tasks);
        }
    }

    render() {

        let tasks = this.state.tasks;
        let message = this.state.message;

        return (
            <div>
                <Message message={message}/>
                {this.getView(tasks)}
            </div>
        )
    }
}

ReactDOM.render(<Priorities/>, document.getElementById('priorities'));