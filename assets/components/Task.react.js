import React from 'react';
import {Draggable} from 'react-beautiful-dnd';

class Task extends React.Component {

    renderTask(task) {
        if (task) {
            return (
                <Draggable key={task.id} draggableId={task.id} index={this.props.index}>
                    {(provided) => (
                        <div className="alert alert-warning "
                             ref={provided.innerRef}
                             {...provided.draggableProps}
                             {...provided.dragHandleProps}
                        >
                            <button type="button" className="close" data-dismiss="alert" aria-label="Close"
                                    onClick={(e) => this.props.removeItem(e, task)}>
                                <span aria-hidden="true">&times;</span>
                            </button>
                            {task.position + ') '} <span dangerouslySetInnerHTML={{__html: task.title}} />
                        </div>
                    )}
                </Draggable>
            )
        }
    }

    render() {
        let task = this.props.task;

        return this.renderTask(task)
    }
}

export default Task;