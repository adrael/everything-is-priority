import React from 'react';
import config from 'react-global-configuration';

const ClientApi = {

    /**
     * Get url for api
     * @returns {string}
     */
    getApiUrl() {
        return config.get('apiEndpoint');
    },

    getHeaders() {
        return {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    },

    /**
     * Get all tasks
     * @returns {Promise<Response | never>}
     */
    getAllTasks() {
        return fetch(`${ClientApi.getApiUrl()}/tasks?hotWordReplace=1`)
            .then(results => {
                return results.json();
            });
    },

    /**
     * Added task to collection
     * @param newTask
     * @returns {Promise<Response>}
     */
    addTask(newTask) {
        return fetch(`${ClientApi.getApiUrl()}/tasks`, {
            method: 'POST',
            headers: ClientApi.getHeaders(),
            body: JSON.stringify(newTask)
        });
    },

    /**
     * Removed task from collection
     * @param task
     * @returns {Promise<Response>}
     */
    removeTask(task) {
        return fetch(`${ClientApi.getApiUrl()}/tasks/${task.id}`, {
            method: 'DELETE',
            headers: ClientApi.getHeaders()
        })
    },

    updateTask(task) {
        return fetch(`${ClientApi.getApiUrl()}/tasks/${task.id}`, {
            method: 'PUT',
            headers: ClientApi.getHeaders(),
            body: JSON.stringify(task)
        })
    },

    updateTaskPosition(task) {
        let taskPosition = {
            position: task.position
        };
        return fetch(`${ClientApi.getApiUrl()}/tasks/${task.id}`, {
            method: 'PATCH',
            headers: ClientApi.getHeaders(),
            body: JSON.stringify(taskPosition)
        })
    },

    //TODO: must send valid json
    createLastPriorityChange() {
        return fetch(`${ClientApi.getApiUrl()}/priority-last-change`, {
            method: 'POST',
            headers: ClientApi.getHeaders()
        })
    },

    /**
     *
     * @returns {Promise<Response | never>}
     */
    getLastTimePriorityChange() {
        return fetch(`${ClientApi.getApiUrl()}/priority-last-change`)
            .then(results => {
                return results.json();
            });
    }
};

export default ClientApi;