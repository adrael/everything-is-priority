import React from 'react';
import {Droppable} from 'react-beautiful-dnd'
import Task from '../components/Task.react';
import config from 'react-global-configuration';

class Board extends React.Component {

    renderBoard(tasks) {
        if (tasks) {
            return (
                <Droppable droppableId="droppable">
                    {(provided) => (
                        <div className="card"
                             ref={provided.innerRef}
                             {...provided.draggableProps}
                             {...provided.dragHandleProps}>

                            <div className="card-body">
                                <h5 className="card-title">{config.get('headerTitle')}</h5> <i onClick={this.props.changeView} className="fa fa-television"/>
                                {tasks.map((task, index) => (
                                    <Task key={task.id} task={task} index={index} removeItem={this.props.removeItem}/>
                                ))}
                                {provided.placeholder}
                            </div>
                        </div>
                    )}
                </Droppable>
            )
        }
    }

    render() {
        let tasks = this.props.tasks;

        return (
            <div>
                {this.renderBoard(tasks)}
            </div>
        )
    }
}

export default Board;