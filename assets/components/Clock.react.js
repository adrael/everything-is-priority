import React from 'react';
import moment from 'moment';

class Clock extends React.Component {

    constructor(props) {
        super(props);
        this.state = {time: 0};
    };

    componentWillUpdate(nextProps, nextState) {
        if (nextProps.clock !== this.state.clock) {
            this.setState({clock: nextProps.clock, time: nextProps.clock.time_diff * 1000});
        }
    }

    tick(clock) {
        if(clock) {
            this.setState({time: moment().unix() - clock.time_last});
        }
    }

    componentDidMount() {
        this.intervalID = setInterval(
            () => this.updateClock(),
            1000
        );
    }

    updateClock() {
        this.tick(this.state.clock);
    }

    componentWillUnmount() {
        clearInterval(this.intervalID);
    }

    getClockFormat() {
        if (this.state.time > 0) {
            return moment(this.state.time).format('mm:ss')
        }
    }

    render() {
        return (
            <span className="priority-clock">{this.getClockFormat()}</span>
        )
    }
}

export default Clock;