import React from 'react';

class Message extends React.Component {

    getMessage() {
        let message = this.props.message;

        if (message) {
            return <div className="alert alert-danger">{message}</div>
        }
    }

    render() {

        return (
            <div className="col-md-12 messages">
                {this.getMessage()}
            </div>
        )
    }
}

export default Message;