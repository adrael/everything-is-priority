<?php

namespace App\Event;

use Symfony\Component\EventDispatcher\Event;

class PriorityChangedEvent extends Event
{
    const NAME = 'priority.changed';
}
