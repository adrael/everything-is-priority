<?php

namespace App\Event;

use App\Service\SlackService;

class PriorityChangedEventListener
{
    private $slackService;

    public function __construct(SlackService $slackService)
    {
        $this->slackService = $slackService;
    }

    public function onPriorityChanged()
    {
        if ((int) getenv('SLACK_INTEGRATION') === 1) {
            $this->slackService->notifyPriorityChanged();
        }
    }
}
