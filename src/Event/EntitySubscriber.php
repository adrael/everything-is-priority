<?php

namespace App\Event;

use App\Service\EntityChangeService;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use App\Entity\Task;

class EntitySubscriber implements EventSubscriber
{
    private $entityChangeService;

    public function __construct(EntityChangeService $entityChangeService)
    {
        $this->entityChangeService = $entityChangeService;
    }

    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
            'postRemove'
        ];
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->process($args);
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        //disable event for doctrine fixtures loading during development
        if ('cli' === php_sapi_name()) {
            return;
        }
        $this->process($args);
    }

    public function postRemove(LifecycleEventArgs $args)
    {
        $this->process($args);
    }

    private function process(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if (! $entity instanceof Task) {
            return;
        }

        $this->entityChangeService->onEntityChange();
    }
}
