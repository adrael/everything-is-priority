<?php

namespace App\DTO;

use JMS\Serializer\Annotation\Type;
use DateTime;

final class DeliveryDTO implements DTOInterface
{
    /**
     * @Type("int")
     */
    private $id;

    /**
     * @Type("string")
     */
    private $name;

    /**
     * @Type("DateTime<'Y-m-d'>")
     */
    private $date;

    /**
     * @Type("int")
     */
    private $position;

    public function __construct(int $id, string $name, DateTime $date, int $position)
    {
        $this->id = $id;
        $this->name = $name;
        $this->date = $date;
        $this->position = $position;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }
}
