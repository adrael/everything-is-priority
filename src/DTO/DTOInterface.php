<?php

namespace App\DTO;

interface DTOInterface
{
    public function setId(int $id);

    public function getId() : int;
}
