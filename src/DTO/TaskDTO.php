<?php

namespace App\DTO;

use JMS\Serializer\Annotation\Type;

final class TaskDTO implements DTOInterface
{
    /**
     * @Type("int")
     */
    private $id;

    /**
     * @Type("string")
     */
    private $title;

    /**
     * @Type("int")
     */
    private $category;

    /**
     * @Type("int")
     */
    private $position;

    public function __construct(int $id, string $title, int $category, int $position)
    {
        $this->id = $id;
        $this->title = $title;
        $this->category = $category;
        $this->position = $position;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function getCategory(): ?int
    {
        return $this->category;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }
}
