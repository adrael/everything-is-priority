<?php

namespace App\DTO;

use JMS\Serializer\Annotation\Type;
use DateTime;

final class PriorityLastChangeDTO implements DTOInterface
{
    /**
     * @Type("int")
     */
    private $id;


    /**
     * @Type("DateTime<'Y-m-d H:i'>")
     */
    private $lastDate;

    public function __construct(int $id, DateTime $lastDate)
    {
        $this->id = $id;
        $this->lastDate = $lastDate;
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getLastDate() : DateTime
    {
        return $this->lastDate;
    }
}
