<?php

namespace App\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
    * @Route("/")
    */
    public function indexAction(): Response
    {
        return $this->render('index.html.twig');
    }
}
