<?php

namespace App\Controller\Rest;

use App\DTO\PriorityLastChangeDTO;
use App\Service\Entity\PriorityLastChangeService;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use DateTime;

class PriorityLastChangeController extends FOSRestController
{
    /**
     * Retrieves a PriorityLastChange last object
     * @Rest\Get("/priority-last-change")
     */
    public function getPriorityLastChange(PriorityLastChangeService $priorityLastChangeService): View
    {
        $priorityLastChange = $priorityLastChangeService->getLast();

        $arrayPriority = $priorityLastChange->toArray();
        $now = new DateTime();
        $dayDiff = $arrayPriority['last_date']->diff($now)->days;
        $arrayPriority['last_date_in_days'] = $dayDiff;

        return View::create($arrayPriority, Response::HTTP_OK);
    }

    /**
     * Creates an PriorityLastChange object
     * @Rest\Post("/priority-last-change")
     * @ParamConverter("priorityLastChangeDTO", converter="fos_rest.request_body")
     * @return View
     */
    public function postPriorityLastChange(
        PriorityLastChangeDTO $priorityLastChangeDTO,
        PriorityLastChangeService $priorityLastChangeService
    ): View {
        return View::create(
            $priorityLastChangeService->create($priorityLastChangeDTO),
            Response::HTTP_CREATED
        );
    }
}
