<?php

namespace App\Controller\Rest;

use App\DTO\DeliveryDTO;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Entity\DeliveryService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class DeliveryController extends FOSRestController
{
    /**
     * Retrieves a collection of Deliveries
     * @Rest\Get("/deliveries")
     * @return View
     */
    public function getDeliveries(DeliveryService $deliveryService): View
    {
        return View::create($deliveryService->getAll(['position' => 'ASC']), Response::HTTP_OK);
    }

    /**
     * Retrieves single delivery
     * @Rest\Get("/deliveries/{deliveryId}")
     * @return View
     */
    public function getDelivery(int $deliveryId, DeliveryService $deliveryService): View
    {
        return View::create($deliveryService->getOneById($deliveryId), Response::HTTP_OK);
    }

    /**
     * Creates an Delivery
     * @Rest\Post("/deliveries")
     * @ParamConverter("deliveryDTO", converter="fos_rest.request_body")
     * @return View
     */
    public function postDelivery(DeliveryDTO $deliveryDTO, DeliveryService $deliveryService): View
    {
        return View::create($deliveryService->create($deliveryDTO), Response::HTTP_CREATED);
    }

    /**
     * Update delivery
     * @Rest\Put("/deliveries/{deliveryId}")
     * @ParamConverter("deliveryDTO", converter="fos_rest.request_body")
     * @return View
     */
    public function putDelivery(int $deliveryId, DeliveryDTO $deliveryDTO, DeliveryService $deliveryService): View
    {
        $deliveryDTO->setId($deliveryId);
        return View::create($deliveryService->updateFull($deliveryDTO), Response::HTTP_OK);
    }

    /**
     * Patch delivery (update single fields in single delivery)
     * @Rest\Patch("/deliveries/{deliveryId}")
     * @ParamConverter("deliveryDTO", converter="fos_rest.request_body")
     * @return View
     */
    public function patchDelivery(int $deliveryId, DeliveryDTO $deliveryDTO, DeliveryService $deliveryService): View
    {
        $deliveryDTO->setId($deliveryId);
        return View::create($deliveryService->updatePartial($deliveryDTO), Response::HTTP_OK);
    }

    /**
     * Removes the Delivery
     * @Rest\Delete("/deliveries/{deliveryId}")
     * @return View
     */
    public function deleteDelivery(int $deliveryId, DeliveryService $deliveryService): View
    {
        $deliveryService->delete($deliveryId);
        return View::create(null, Response::HTTP_NO_CONTENT);
    }
}
