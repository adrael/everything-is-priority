<?php

namespace App\Controller\Rest;

use App\DTO\TaskDTO;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Service\Entity\TaskService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class TaskController extends FOSRestController
{
    /**
     * Retrieves a collection of Tasks
     * @Rest\Get("/tasks")
     * @return View
     */
    public function getTasks(TaskService $taskService, Request $request): View
    {
        $hotWordReplace = boolval($request->get('hotWordReplace'));
        return View::create(
            $taskService->getAll(['position' => 'ASC'], $hotWordReplace),
            Response::HTTP_OK
        );
    }

    /**
     * Retrieves single task
     * @Rest\Get("/tasks/{taskId}")
     * @return View
     */
    public function getTask(int $taskId, TaskService $taskService, Request $request): View
    {
        $hotWordReplace = boolval($request->get('hotWordReplace'));
        return View::create($taskService->getOneById($taskId, $hotWordReplace), Response::HTTP_OK);
    }

    /**
     * Creates an Task
     * @Rest\Post("/tasks")
     * @ParamConverter("taskDTO", converter="fos_rest.request_body")
     * @return View
     */
    public function postTask(TaskDTO $taskDTO, TaskService $taskService): View
    {
        return View::create($taskService->create($taskDTO), Response::HTTP_CREATED);
    }

    /**
     * Update task
     * @Rest\Put("/tasks/{taskId}")
     * @ParamConverter("taskDTO", converter="fos_rest.request_body")
     * @return View
     */
    public function putTask(int $taskId, TaskDTO $taskDTO, TaskService $taskService): View
    {
        $taskDTO->setId($taskId);
        return View::create($taskService->updateFull($taskDTO), Response::HTTP_OK);
    }

    /**
     * Patch task (update single fields in single task)
     * @Rest\Patch("/tasks/{taskId}")
     * @ParamConverter("taskDTO", converter="fos_rest.request_body")
     * @return View
     */
    public function patchTask(int $taskId, TaskDTO $taskDTO, TaskService $taskService): View
    {
        $taskDTO->setId($taskId);
        return View::create($taskService->updatePartial($taskDTO), Response::HTTP_OK);
    }

    /**
     * Removes the Task
     * @Rest\Delete("/tasks/{taskId}")
     * @return View
     */
    public function deleteTask(int $taskId, TaskService $taskService): View
    {
        $taskService->delete($taskId);
        return View::create(null, Response::HTTP_NO_CONTENT);
    }
}
