<?php

namespace App\Service;

use App\Event\PriorityChangedEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class EntityChangeService
{
    private $priorityFile;

    private $priorityChangedEvent;

    private $eventDispatcher;

    public function __construct(
        string $projectDir,
        PriorityChangedEvent $priorityChangedEvent,
        EventDispatcherInterface $eventDispatcher
    ) {
        $this->priorityFile = $projectDir . '/var/priorityLastChange.txt';
        $this->priorityChangedEvent = $priorityChangedEvent;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function onEntityChange()
    {
        $lastChange = $this->getLastChange();
        $minTime = $this->getMinimumUpdateTime();

        if ($lastChange < $minTime) {
            $this->updatePriorityFile();
            $this->eventDispatcher->dispatch(PriorityChangedEvent::NAME, $this->priorityChangedEvent);
        }
    }

    private function getLastChange() : int
    {
        if (! file_exists($this->priorityFile)) {
            touch($this->priorityFile);
        }
        return (int) file_get_contents($this->priorityFile);
    }

    private function getMinimumUpdateTime() : int
    {
        return time() - (int) getenv('PRIORITY_UPDATE_MIN_TIME');
    }

    private function updatePriorityFile()
    {
        file_put_contents($this->priorityFile, time());
    }
}
