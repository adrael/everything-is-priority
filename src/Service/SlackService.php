<?php

namespace App\Service;

use Nexy\Slack\Attachment;
use Nexy\Slack\Client as SlackClient;
use App\Client\CatApiClient;

class SlackService
{
    private $slackClient;

    private $catApiClient;

    private $attachment;

    public function __construct(SlackClient $slackClient, Attachment $attachment, CatApiClient $catApiClient)
    {
        $this->slackClient = $slackClient;
        $this->attachment = $attachment;
        $this->catApiClient = $catApiClient;
    }

    public function notifyPriorityChanged()
    {

        if ((int) getenv('SLACK_RANDOM_CATS') === 1) {
            $randomCat = $this->catApiClient->getRandomCat();
            if ($randomCat) {
                $this->attachment->setImageUrl($randomCat);
            }
        }
        $this->slackClient->attach($this->attachment)->send(getenv('SLACK_MESSAGE'));
    }
}
