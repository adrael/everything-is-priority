<?php

namespace App\Service\Entity;

use App\DTO\DTOInterface;
use Doctrine\ORM\EntityNotFoundException;

class AbstractEntityService
{
    protected $repository;

    protected $entityManager;

    public function getAll(array $order = []) : array
    {
        return $this->repository->findBy([], $order);
    }

    public function getOneById(int $id)
    {
        $entity = $this->repository->findOneById($id);
        if (! $entity) {
            $this->throwEntityNotFound($id);
        }

        return $entity;
    }

    protected function update(DTOInterface $DTO)
    {
        $entity = $this->getOneById($DTO->getId());
        $this->populate($entity, $DTO);
        return $this->save($entity);
    }

    public function delete(int $id)
    {
        $entity = $this->repository->findOneById($id);
        if (! $entity) {
            $this->throwEntityNotFound($id);
        }

        $this->entityManager->remove($entity);
        $this->entityManager->flush();
    }

    protected function save($entity)
    {
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return $entity;
    }

    protected function throwEntityNotFound(int $id, string $entityName = null)
    {
        if (! $entityName) {
            $entityName = $this->repository->getClassName();
        }
        throw new EntityNotFoundException($entityName . ' with id ' . $id . ' does not exist');
    }
}
