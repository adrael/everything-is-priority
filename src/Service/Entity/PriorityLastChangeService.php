<?php

namespace App\Service\Entity;

use App\DTO\PriorityLastChangeDTO;
use App\Entity\PriorityLastChange;
use App\Repository\PriorityLastChangeRepository;
use Doctrine\ORM\EntityManagerInterface;
use TypeError;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Doctrine\ORM\EntityNotFoundException;

class PriorityLastChangeService extends AbstractEntityService
{
    public function __construct(
        PriorityLastChangeRepository $repository,
        EntityManagerInterface $entityManager
    ) {
        $this->repository = $repository;
        $this->entityManager = $entityManager;
    }
    
    public function getLast() : PriorityLastChange
    {
        $priorityLastChange = $this->repository->findOneBy([], ['id' => 'DESC']);

        if (! $priorityLastChange) {
            $this->throwEntityNotFound(0);
        }
        return $priorityLastChange;
    }

    public function create(PriorityLastChangeDTO $priorityLastChangeDTO) : PriorityLastChange
    {
        $priorityLastChange = $this->populate(new PriorityLastChange(), $priorityLastChangeDTO);
        return $this->save($priorityLastChange);
    }

    protected function populate(
        PriorityLastChange $priorityLastChange,
        PriorityLastChangeDTO $priorityLastChangeDTO
    ) : PriorityLastChange {
        try {
            $priorityLastChangeDTO->getLastDate() ?
                $priorityLastChange->setLastDate($priorityLastChangeDTO->getLastDate()) : null;
        } catch (TypeError $error) {
            throw new BadRequestHttpException('Invalid data type or invalid field name');
        }

        return $priorityLastChange;
    }

    protected function throwEntityNotFound(int $id, string $entityName = null)
    {
        throw new EntityNotFoundException('There is no priorityLastChange records, create first one');
    }
}
