<?php

namespace App\Service\Entity;

use App\DTO\TaskDTO;
use App\Entity\Task;
use App\Repository\CategoryRepository;
use App\Repository\TaskRepository;
use App\String\Replacer\HotWordStringReplacer;
use Doctrine\ORM\EntityManagerInterface;
use TypeError;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class TaskService extends AbstractEntityService
{
    private $categoryRepository;

    public function __construct(
        TaskRepository $repository,
        CategoryRepository $categoryRepository,
        EntityManagerInterface $entityManager
    ) {
        $this->repository = $repository;
        $this->categoryRepository = $categoryRepository;
        $this->entityManager = $entityManager;
    }

    public function getAll(array $order = [], bool $hotWordReplace = false): array
    {
        $tasks = parent::getAll($order);
        if (true === boolval(getenv('HOT_WORD_ENABLED')) && $hotWordReplace) {
            foreach ($tasks as $task) {
                $this->replaceTaskTitle($task);
            }
        }
        return $tasks;
    }

    public function getOneById(int $id, bool $hotWordReplace = false)
    {
        $task = parent::getOneById($id);
        if (true === boolval(getenv('HOT_WORD_ENABLED')) && $hotWordReplace) {
            $this->replaceTaskTitle($task);
        }
        return $task;
    }

    public function create(TaskDTO $taskDTO) : Task
    {
        $task = $this->populate(new Task(), $taskDTO);
        return $this->save($task);
    }

    public function updateFull(TaskDTO $taskDTO) : Task
    {
        $isAllFieldsSet =
            $taskDTO->getId() && $taskDTO->getTitle() && $taskDTO->getPosition() && $taskDTO->getCategory();
        if (! $isAllFieldsSet) {
            throw new BadRequestHttpException('Invalid data type or invalid field name or not all fields set');
        }

        return $this->update($taskDTO);
    }

    public function updatePartial(TaskDTO $taskDTO) : Task
    {
        return $this->update($taskDTO);
    }

    protected function populate(Task $task, TaskDTO $taskDTO) : Task
    {
        try {
            if ($taskDTO->getCategory()) {
                $category = $this->categoryRepository->findOneById($taskDTO->getCategory());
                if (! $category) {
                    $this->throwEntityNotFound($taskDTO->getCategory(), 'Category');
                }
                $task->setCategory($category);
            }
            $taskDTO->getTitle() ? $task->setTitle($taskDTO->getTitle()) : null;
            $taskDTO->getPosition() ? $task->setPosition($taskDTO->getPosition()) : null;
        } catch (TypeError $error) {
            throw new BadRequestHttpException('Invalid data type or invalid field name');
        }

        return $task;
    }

    private function replaceTaskTitle(Task $task)
    {
        $pattern = getenv('HOT_WORD_PATTERN');
        $replacement = getenv('HOT_WORD_REPLACEMENT');
        $task->setTitle(HotWordStringReplacer::replace($pattern, $replacement, $task->getTitle()));
    }
}
