<?php

namespace App\Service\Entity;

use App\DTO\DeliveryDTO;
use App\Entity\Delivery;
use App\Repository\DeliveryRepository;
use Doctrine\ORM\EntityManagerInterface;
use TypeError;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class DeliveryService extends AbstractEntityService
{
    public function __construct(DeliveryRepository $repository, EntityManagerInterface $entityManager)
    {
        $this->repository = $repository;
        $this->entityManager = $entityManager;
    }

    public function create(DeliveryDTO $deliveryDTO) : Delivery
    {
        $delivery = $this->populate(new Delivery(), $deliveryDTO);
        return $this->save($delivery);
    }

    public function updateFull(DeliveryDTO $deliveryDTO) : Delivery
    {
        $isAllFieldsSet =
            $deliveryDTO->getId() && $deliveryDTO->getName() && $deliveryDTO->getDate() && $deliveryDTO->getPosition();
        if (! $isAllFieldsSet) {
            throw new BadRequestHttpException('Invalid data type or invalid field name or not all fields set');
        }

        return $this->update($deliveryDTO);
    }

    public function updatePartial(DeliveryDTO $deliveryDTO) : Delivery
    {
        return $this->update($deliveryDTO);
    }

    protected function populate(Delivery $delivery, DeliveryDTO $deliveryDTO) : Delivery
    {
        try {
            $deliveryDTO->getName() ? $delivery->setName($deliveryDTO->getName()) : null;
            $deliveryDTO->getDate() ? $delivery->setDate($deliveryDTO->getDate()) : null;
            $deliveryDTO->getPosition() ? $delivery->setPosition($deliveryDTO->getPosition()) : null;
        } catch (TypeError $error) {
            throw new BadRequestHttpException('Invalid data type or invalid field name');
        }

        return $delivery;
    }
}
