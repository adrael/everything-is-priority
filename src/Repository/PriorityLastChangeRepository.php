<?php

namespace App\Repository;

use App\Entity\PriorityLastChange;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PriorityLastChange|null find($id, $lockMode = null, $lockVersion = null)
 * @method PriorityLastChange|null findOneBy(array $criteria, array $orderBy = null)
 * @method PriorityLastChange[]    findAll()
 * @method PriorityLastChange[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PriorityLastChangeRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PriorityLastChange::class);
    }

//    /**
//     * @return PriorityLastChange[] Returns an array of PriorityLastChange objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PriorityLastChange
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
