<?php

namespace App\Client;

use GuzzleHttp\Client as HttpClient;

class CatApiClient
{
    const RANDOM_CAT_URL = 'https://api.thecatapi.com/v1/images/search?size=medium&limit=1';

    private $httpClient;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function getRandomCat() : ?string
    {
        $res = $this->httpClient->request('GET', self::RANDOM_CAT_URL);
        if ($res->getStatusCode() !== 200) {
            return null;
        }
        return json_decode($res->getBody())[0]->url;
    }
}
