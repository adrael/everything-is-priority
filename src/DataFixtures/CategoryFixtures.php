<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{
    const CATEGORY_1_REFERENCE = 'category-1-reference';
    const CATEGORY_2_REFERENCE = 'category-2-reference';
    const CATEGORY_3_REFERENCE = 'category-3-reference';

    public function load(ObjectManager $manager)
    {
        $category1 = new Category();
        $category1->setName('Category 1');
        $manager->persist($category1);

        $category2 = new Category();
        $category2->setName('Category 2');
        $manager->persist($category2);

        $category3 = new Category();
        $category3->setName('Category 3');
        $manager->persist($category3);

        $manager->flush();

        $this->addReference(self::CATEGORY_1_REFERENCE, $category1);
        $this->addReference(self::CATEGORY_2_REFERENCE, $category2);
        $this->addReference(self::CATEGORY_3_REFERENCE, $category3);
    }
}
