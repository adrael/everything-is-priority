<?php

namespace App\DataFixtures;

use App\Entity\Delivery;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class DeliveryFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $delivery = new Delivery();
        $delivery->setName('Delivery 1');
        $delivery->setDate(new \DateTime());
        $delivery->setPosition(1);
        $manager->persist($delivery);

        $delivery = new Delivery();
        $delivery->setName('Delivery 2');
        $delivery->setDate(new \DateTime());
        $delivery->setPosition(2);
        $manager->persist($delivery);

        $delivery = new Delivery();
        $delivery->setName('Delivery 3');
        $delivery->setDate(new \DateTime());
        $delivery->setPosition(3);
        $manager->persist($delivery);

        $manager->flush();
    }
}
