<?php

namespace App\DataFixtures;

use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class TaskFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $task = new Task();
        $task->setTitle('Example task 1');
        $task->setCategory($this->getReference(CategoryFixtures::CATEGORY_1_REFERENCE));
        $task->setPosition(1);
        $manager->persist($task);

        $task = new Task();
        $task->setTitle('Example task 2');
        $task->setCategory($this->getReference(CategoryFixtures::CATEGORY_1_REFERENCE));
        $task->setPosition(2);
        $manager->persist($task);

        $task = new Task();
        $task->setTitle('Example of conversion hot-word to link: example-text');
        $task->setCategory($this->getReference(CategoryFixtures::CATEGORY_1_REFERENCE));
        $task->setPosition(2);
        $manager->persist($task);

        $task = new Task();
        $task->setTitle('Lorem ipsum');
        $task->setCategory($this->getReference(CategoryFixtures::CATEGORY_2_REFERENCE));
        $task->setPosition(3);
        $manager->persist($task);

        $task = new Task();
        $task->setTitle('dolor sit amet');
        $task->setCategory($this->getReference(CategoryFixtures::CATEGORY_3_REFERENCE));
        $task->setPosition(4);
        $manager->persist($task);

        $task = new Task();
        $task->setTitle('New fancy task, specially for you');
        $task->setCategory($this->getReference(CategoryFixtures::CATEGORY_2_REFERENCE));
        $task->setPosition(5);
        $manager->persist($task);

        $task = new Task();
        $task->setTitle('I have no idea what to put here');
        $task->setCategory($this->getReference(CategoryFixtures::CATEGORY_3_REFERENCE));
        $task->setPosition(6);
        $manager->persist($task);

        $task = new Task();
        $task->setTitle('Silly task');
        $task->setCategory($this->getReference(CategoryFixtures::CATEGORY_1_REFERENCE));
        $task->setPosition(7);
        $manager->persist($task);

        $manager->flush();
    }

    public function getDependencies()
    {
        return [
            CategoryFixtures::class
        ];
    }
}
