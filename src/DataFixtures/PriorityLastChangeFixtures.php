<?php

namespace App\DataFixtures;

use App\Entity\PriorityLastChange;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PriorityLastChangeFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $priorityLastChange = new PriorityLastChange();
        $priorityLastChange->setLastDate(new \DateTime());
        $manager->persist($priorityLastChange);

        $priorityLastChange = new PriorityLastChange();
        $priorityLastChange->setLastDate(new \DateTime());
        $manager->persist($priorityLastChange);

        $priorityLastChange = new PriorityLastChange();
        $priorityLastChange->setLastDate(new \DateTime());
        $manager->persist($priorityLastChange);

        $priorityLastChange = new PriorityLastChange();
        $priorityLastChange->setLastDate(new \DateTime());
        $manager->persist($priorityLastChange);

        $manager->flush();
    }
}
