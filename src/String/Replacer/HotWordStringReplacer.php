<?php

namespace App\String\Replacer;

class HotWordStringReplacer
{
    public static function replace(string $pattern, string $replacement, string $subject) : string
    {
        preg_match_all($pattern, $subject, $matches);

        foreach ($matches[0] as $match) {
            $subject = str_replace($match, $replacement, $subject);
            $subject = str_replace('<hot_word>', $match, $subject);
        }

        return $subject;
    }
}
