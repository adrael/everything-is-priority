# Everyting is priority

Application to manage project priorities. In some specific projects JIRA or Trello board is not enough. Priorities changes rapidly and you feel lost in the maze of labels, deliveries, e-mails and conversations.

This app helps to present most important "priorities of priorities of priorities" in a simple way. You can just launch it on your office TV or monitor, or just give Application Url to project participants and make most important things clear.

### Features
- Show priority Tasks in normal and fullscreen view
- Live updates on the screen
- Slack notifications about changes (with random cat with each notification! Thanks to [https://thecatapi.com](https://thecatapi.com))

### Prerequisites
You need the following stuff to develop or run this application:
- PHP ^7.2
- npm ~3.5
- Apache2 server
- MariaDb or MySQL database (any not "museum" version should be enough)
- Also you need to have [composer](https://getcomposer.org/) and [yarn](https://yarnpkg.com) installed on your system


### Installing

To run this application in development mode

1) Create configuration files:
    - copy file ```env.dist``` file and name it ```.env```. In typical case you only need:
        - update ```DATABASE_URL```
        - Enable or disable slack integration with ```SLACK_INTEGRATION```
        - If slack integration is enabled, set ```SLACK_HOOK``` (see [here](https://api.slack.com/incoming-webhooks))
    - copy file ```config.dist.js``` file and name it ```config.js```. This file contains only backend API url. In typical case this url should looks like ```http://your_instance_url/api```.

2) Install all dependencies automatically: this will automatically install all dependencies and create example database schema. Just run:
    ```
    composer install-dev
    ```
    If you prefer to install all dependencies manually, go to next step.

3) Install all dependencies manually (please omit this step if you prefer step 2)
    - run ```composer install```
    - run ```npm install```
    - run ```yarn install```
    - run ```yarn encore dev```
    - if you are an frontend developer and you want to automatically listen and refresh all frontend changes, run ``yarn encore dev --watch``. Full documentation for Encore is available [here](https://symfony.com/doc/current/frontend.html)

4) If you want to re-create your database schema with example data just run:
``composer reload-db``


## Contributing
Project is on very first stage, so all contributions are welcome. Just follow coding standards and good practices.

## Authors

* **Łukasz Tarnowski** - *Initial API / backend work and project maintenance*
* **Paweł Piechociński** - *Initial frontend work and project maintenance*
* **Patryk Wojtasik** - *Ideas, improvements and hopefully development ;-)*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* [Symfony](https://symfony.com/) authors, for great PHP frameork
* [React](https://reactjs.org/) authors, for great frontend framework
* [The Cat Api](https://www.programmableweb.com/api/cat) author for the best cat API
